﻿#pragma comment(lib, "ws2_32.lib")
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <stdio.h>
#include <winsock2.h>
#include <windows.h>

#define BUFFER_SIZE	256
#define SOCKET_PORT	23875

SOCKET echo_sock;

void exit_handler()
{
	printf("Server shutdown...\n");
	closesocket(echo_sock);
	WSACleanup();
}

int main()
{
	SOCKADDR_IN socket_addr;
	SOCKADDR_IN remote_addr;
	int sa_len = sizeof(socket_addr);
	int ra_len = sizeof(remote_addr);
	memset(&socket_addr, 0, sa_len);
	memset(&remote_addr, 0, ra_len);

	atexit(exit_handler);

	WSADATA ws;
	if (WSAStartup(MAKEWORD(2, 2), &ws))
	{
		printf("Error init of WinSock2\n");
		return -1;
	}

	if ((echo_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) <= 0)
	{
		printf("Can't create socket\n");
		return -1;
	}

	socket_addr.sin_family = AF_INET;
	socket_addr.sin_port = htons(SOCKET_PORT);
	socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(echo_sock, (SOCKADDR *)&socket_addr, sizeof(socket_addr)) != 0)
	{
		printf("Can't bind socket to the port %d\n", SOCKET_PORT);
		return -1;
	}

	printf("Ready incoming messages...\n");
	do
	{
		char dataBuffer[BUFFER_SIZE];

		int msg_len = recvfrom(echo_sock, dataBuffer, BUFFER_SIZE - 1, 0, (SOCKADDR *)&remote_addr, &ra_len);
		printf("[%s:%d] %s\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port), dataBuffer);

		printf("Sending %d bytes to %s:%d...\n", msg_len, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
		msg_len = sendto(echo_sock, dataBuffer, msg_len, 0, (SOCKADDR *)&remote_addr, ra_len);

		if (msg_len <= 0)
			printf("Can\'t send data\n");
		else
			printf("%d bytes sent\n", msg_len);

	} while (1);

	return 0;
}