﻿#pragma comment(lib, "ws2_32.lib")
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <winsock2.h>
#include <windows.h>
#include <iostream>

using namespace std;

#define SOCKET_PORT	23875

SOCKET echo_sock;

void exit_handler()
{
	closesocket(echo_sock);
	WSACleanup();
}

int main(int argc, char **argv)
{
    atexit(exit_handler);

	SOCKADDR_IN socket_addr;
	SOCKADDR_IN remote_addr;
	int sa_len = sizeof(socket_addr);
	int ra_len = sizeof(remote_addr);
	memset(&socket_addr, 0, sa_len);
	memset(&remote_addr, 0, ra_len);

	WSADATA ws;
	if (WSAStartup(MAKEWORD(2, 2), &ws))
	{
		cout << "Error init of WinSock2" << endl;
		return -1;
	}

	if ((echo_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) <= 0)
	{
		cout << "Can't create socket" << endl;
		return -1;
	}

	if (argc != 2)
	{
		cout << "Usage: " << argv[0] << " <Host>" << endl;
		return -1;
	}

	socket_addr.sin_family = AF_INET;
	socket_addr.sin_port = htons(0);
	socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(echo_sock, (SOCKADDR *)&socket_addr, sizeof(socket_addr)) != 0)
	{
		printf("Can't bind socket to the port %d\n", SOCKET_PORT);
		return -1;
	}

	remote_addr.sin_family = AF_INET;
	remote_addr.sin_port = htons(SOCKET_PORT);
	remote_addr.sin_addr.s_addr = inet_addr(argv[1]);

	for (int i = 0; i < 32; ++i)
	{
		char msg[] = "#### Test message ####";

		int len = sendto(echo_sock, msg, sizeof(msg), 0, (SOCKADDR *)&remote_addr, ra_len);
		if (len <= 0)
		{
			cout << "Can\'t send data to the server" << endl;
			cout << "Error #" << WSAGetLastError() << endl;
			break;
		}
		cout << len << " bytes sent" << endl;

		char buf[256];
		len = recvfrom(echo_sock, buf, sizeof(buf), 0, (SOCKADDR *)&remote_addr, &ra_len);
		cout << "Received from " << inet_ntoa(remote_addr.sin_addr) << ":" << ntohs(remote_addr.sin_port) << " - " << buf << endl;
	}

	return 0;
}